extends Node

@export var score = 0

signal score_updated(int)

func _on_mob_squashed():
	print(score)
	score += 1
	score_updated.emit(score)

func init():
	score = 0
