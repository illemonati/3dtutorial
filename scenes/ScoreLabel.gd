extends Label

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	$/root/PlayerData.score_updated.connect(_on_score_updated.bind())
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_score_updated(score: int):
	text = "%d" % score
