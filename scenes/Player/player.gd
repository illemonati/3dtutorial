extends CharacterBody3D

@export var speed = 14
@export var fall_acceleration = 75 # ie, gravity
@export var jump_impulse = 20
@export var bounce_impulse = 16

signal hit

var target_velocity = Vector3.ZERO



func _physics_process(delta):
	var direction = Vector3.ZERO
	if Input.is_action_pressed("move_right"):
		direction.x += 1
	if Input.is_action_pressed("move_left"):
		direction.x -= 1
	if Input.is_action_pressed("move_back"):
		direction.z += 1
	if Input.is_action_pressed("move_forward"):
		direction.z -= 1
		
	if direction != Vector3.ZERO:
		direction = direction.normalized()
		$Pivot.look_at(position + direction, Vector3.UP)
		$AnimationPlayer.speed_scale = 4
	else:
		$AnimationPlayer.speed_scale = 1
	
	target_velocity.x = direction.x * speed
	target_velocity.z = direction.z * speed
	
	if is_on_floor():
		target_velocity.y = 0
		if Input.is_action_just_pressed("jump"):
			target_velocity.y = jump_impulse
	else:
		target_velocity.y = target_velocity.y - (fall_acceleration * delta)
		
	
	velocity = target_velocity
	
	
	for i in range(get_slide_collision_count()):
		var collision = get_slide_collision(i)
		
		if collision.get_collider() == null:
			continue
		
		if collision.get_collider().is_in_group("world"):
			continue
		
		if collision.get_collider().is_in_group("mobs"):
			var mob = collision.get_collider()
			var above = Vector3.UP.dot(collision.get_normal()) 
			if above > 0.1:
				mob.squash()
				target_velocity.y = bounce_impulse
				break
	
	$Pivot.rotation.x = PI/6 * velocity.y / jump_impulse
	
	move_and_slide()

func die():
	hit.emit()
	queue_free()


func _on_mob_detector_body_entered(body):
	die()
