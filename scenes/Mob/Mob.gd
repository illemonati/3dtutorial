extends CharacterBody3D

@export var min_speed = 10
@export var max_speed = 18

signal squashed

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")


func _physics_process(delta):
	move_and_slide()

func initialize(start_position: Vector3, player_position: Vector3):
	var random_speed = randi_range(min_speed, max_speed)
	$AnimationPlayer.speed_scale = random_speed / min_speed
	look_at_from_position(start_position, player_position, Vector3.UP)
	
	rotate_y(randf_range(-PI / 4, PI /4))
	

	velocity = Vector3.FORWARD * random_speed
	velocity = velocity.rotated(Vector3.UP, rotation.y)


func _on_visible_on_screen_notifier_3d_screen_exited():
	queue_free()

func squash():
	squashed.emit()
	queue_free()
