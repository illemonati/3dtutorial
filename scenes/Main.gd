extends Node

@export var mob_scene: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	$UserInterface/Retry.hide()
	$"/root/PlayerData".init()
	$"/root/PlayerData".score_updated.connect(_on_score_updated.bind())

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_mob_timer_timeout():
	var mob = mob_scene.instantiate()
	var mob_spawn_location = get_node("SpawnPath/SpawnLocation")
	mob_spawn_location.progress_ratio = randf()
	
	var player_position = $Player.position
	mob.initialize(mob_spawn_location.position + Vector3(0, 1.5, 0), player_position)
	
	mob.squashed.connect($"/root/PlayerData"._on_mob_squashed.bind())
	
	add_child(mob)


func _on_score_updated(score: int):
	$MobTimer.wait_time = max($MobTimer.wait_time * 2 / 3, 0.005)

func _on_player_hit():
	$MobTimer.stop()
	$UserInterface/Retry.show()

func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") and $UserInterface/Retry.is_visible():
		get_tree().reload_current_scene()
